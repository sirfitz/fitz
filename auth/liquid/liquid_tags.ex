defmodule Fluid do

  def filter(body) do
    illegal =
      Regex.scan(~r/\{%(.*?)\%}/, body)
      |> List.flatten
      |> Enum.filter(fn x ->
        !String.starts_with?(x, tags) && String.contains?(x, "{%")
       end)

    body =
      if illegal && illegal != [] do
        String.replace(body, illegal, "")
      else
        body
      end
  end

  def tags do
  ["{% assign",
   "{% case",
   "{% cycle",
   "{% comment",
   "{% capture",
   "{% continue",
   "{% collection",
   "{% decrement",
   "{% else",
   "{% endif",
   "{% elsif",
   "{% endraw",
   "{% endfor",
   "{% endcase",
   "{% endunless",
   "{% endcomment",
   "{% endtablerow",
   "{% if",
   "{% for",
   "{% include",
   "{% increment",
   "{% partial",
   "{% tablerow",
   "{% raw",
   "{% unless",
   "{% when",
   "{%assign",
   "{%case",
    "{%cycle",
    "{%comment",
    "{%capture",
    "{%continue",
    "{%collection",
    "{%decrement",
    "{%else",
    "{%endif",
    "{%elsif",
    "{%endraw",
    "{%endfor",
    "{%endcase",
    "{%endunless",
    "{%endcomment",
    "{%endtablerow",
    "{%if",
    "{%for",
    "{%include",
    "{%increment",
    "{%partial",
    "{%tablerow",
    "{%raw",
    "{%unless",
    "{%when"
    ]
  end

end
