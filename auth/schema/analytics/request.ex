defmodule Data.Analytics.Request do
  use Ecto.Schema
  import Ecto.Changeset


  schema "requests" do
    field :headers, :map
    field :referer, :string
    field :ip, :string
    field :user_agent, :string
    field :visitor_id, :string
    field :request_path, :string
    field :site_id, :integer

    timestamps()
  end

  @doc false
  def changeset(request, attrs) do
    request
    |> cast(attrs, [:user_agent, :ip, :site_id, :request_path, :visitor_id, :headers, :referer])
    |> validate_required([:user_agent, :request_path, :ip, :visitor_id, :headers])
  end
end
