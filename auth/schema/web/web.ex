defmodule Data.Web do
  @moduledoc """
  The Web context.
  """

  import Ecto.Query, warn: false
  alias Data.Repo

  alias Data.Web.Site

  @doc """
  Returns the list of sites.

  ## Examples

      iex> list_sites()
      [%Site{}, ...]

  """
  def list_sites do
    Repo.all(Site)
  end

  @doc """
  Gets a single site.

  Raises `Ecto.NoResultsError` if the Site does not exist.

  ## Examples

      iex> get_site!(123)
      %Site{}

      iex> get_site!(456)
      ** (Ecto.NoResultsError)

  """
  def get_site!(id), do: Repo.get!(Site, id)

  @doc """
  Creates a site.

  ## Examples

      iex> create_site(%{field: value})
      {:ok, %Site{}}

      iex> create_site(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_site(attrs \\ %{}) do
    case %Site{}
          |> Site.changeset(attrs)
          |> Repo.insert() do
      {:ok, site} ->
          IO.inspect site
          IO.inspect create_site_user(%{"site_id" => site.id, "user_id" => attrs["owner_id"], "role" => 5, "referer_id" => attrs["referer_id"]})
          {:ok, site}
      {:error, changeset} ->
          {:error, changeset}
    end
  end

  @doc """
  Updates a site.

  ## Examples

      iex> update_site(site, %{field: new_value})
      {:ok, %Site{}}

      iex> update_site(site, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_site(%Site{} = site, attrs) do
    site
    |> Site.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Site.

  ## Examples

      iex> delete_site(site)
      {:ok, %Site{}}

      iex> delete_site(site)
      {:error, %Ecto.Changeset{}}

  """
  def delete_site(%Site{} = site) do
    Repo.delete(site)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking site changes.

  ## Examples

      iex> change_site(site)
      %Ecto.Changeset{source: %Site{}}

  """
  def change_site(%Site{} = site) do
    Site.changeset(site, %{})
  end

  alias Data.Web.SiteUser

  @doc """
  Returns the list of site_users.

  ## Examples

      iex> list_site_users()
      [%SiteUser{}, ...]

  """
  def list_site_users(conn, params) do
    site =  conn.assigns[:current_team]
    SiteUser
    |> where([u], u.site_id == ^site.id)
    |> preload([:user])
    |> Repo.all()
  end

  @doc """
  Gets a single site_user.

  Raises `Ecto.NoResultsError` if the Site user does not exist.

  ## Examples

      iex> get_site_user!(123)
      %SiteUser{}

      iex> get_site_user!(456)
      ** (Ecto.NoResultsError)

  """
  #def get_site_user!(id), do: Repo.get!(SiteUser, id)

  def get_site_user(site_id, user_id) do
    SiteUser
    |> where([su], su.user_id == ^user_id and su.site_id == ^site_id)
    |> limit(1)
    |> Repo.one
  end

  @doc """
  Creates a site_user.

  ## Examples

      iex> create_site_user(%{field: value})
      {:ok, %SiteUser{}}

      iex> create_site_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_site_user(attrs \\ %{}) do
    %SiteUser{}
    |> SiteUser.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a site_user.

  ## Examples

      iex> update_site_user(site_user, %{field: new_value})
      {:ok, %SiteUser{}}

      iex> update_site_user(site_user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_site_user(%SiteUser{} = site_user, attrs) do
    site_user
    |> SiteUser.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a SiteUser.

  ## Examples

      iex> delete_site_user(site_user)
      {:ok, %SiteUser{}}

      iex> delete_site_user(site_user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_site_user(%SiteUser{} = site_user) do
    Repo.delete(site_user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking site_user changes.

  ## Examples

      iex> change_site_user(site_user)
      %Ecto.Changeset{source: %SiteUser{}}

  """
  def change_site_user(%SiteUser{} = site_user) do
    SiteUser.changeset(site_user, %{})
  end

end
