defmodule Test.LiquidFilters do
  def custom_date(date) do
    Timex.format!(date, "%d.%m.%Y", :strftime)
  end

  def resize(image, size, gravity \\ nil) do
    IO.puts "FILTERING"
    IO.inspect size
    "/flow/#{size}#{if gravity do gravity <> "/" else "/" end}#{image}"
  end

  def add_ten(value) do
    value + 10
  end
end

defmodule Test do
  def run do
    Liquid.Filters.add_filters(Test.LiquidFilters)

    data =
      %{
        "my_date" => Ecto.DateTime.utc(),
        "my_int" => 32,
        "image" => "http://api.romariofitzgerald.com/logo.png",
        "site" => %{id: 1}
      }

    template =
      """
        {{ my_date | custom_date }} <br />
        {{ my_int | add_ten }} <br/>
        {{ image | resize: '100x100' }}
        {% collection blogs order_by: desc id, limit:2 %}
      """

    {:ok, output, _} =
      template   |> Liquid.Template.parse |> Liquid.Template.render(data)

    output
  end
end
