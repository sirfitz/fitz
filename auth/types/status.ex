defmodule Data.Status do

    def posts do
        [
          %{
          "id" => 1,
          "name" => "Published"
          },
          %{
            "id" => 2,
            "name" => "Draft"
          },
          %{
            "id" => 3,
            "name" => "Private"
          },
          %{
            "id" => 4,
            "name" => "Expired"
          },
          %{
            "id" => 5,
            "name" => "Out Of Stock"
          }
        ]
    end

end
