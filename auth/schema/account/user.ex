defmodule Data.Account.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :about, :string
    field :address, :string
    field :age, :integer
    field :city, :string
    field :country, :string
    field :dob, :date
    field :email, :string
    field :first_name, :string
    field :site_id, :integer
    field :gender, :string
    field :ip_address, :string
    field :last_acitivty, :naive_datetime
    field :last_login, :naive_datetime
    field :last_name, :string
    field :name, :string
    field :organization, :string
    field :password, :string
    field :phone, :string
    field :photo, :string
    field :postal_code, :string
    field :referer_id, :integer
    field :region, :string
    field :role, :integer
    field :settings, :map
    field :social, :map
    field :status, :integer
    field :strikes, :integer
    field :teams, {:array, :integer}
    field :title, :string
    field :type, :string
    field :uid, :string
    field :username, :string
    field :verified_at, :naive_datetime

    timestamps()
  end

  @required [:email, :password]

  @optional [:first_name, :last_name, :name, :age, :gender, :dob, :phone,
             :title, :username, :role, :photo, :about, :social, :site_id,
             :organization, :address, :city, :region, :country, :postal_code,
             :ip_address, :referer_id, :status, :type, :strikes, :settings,
             :verified_at, :last_login, :uid, :teams, :last_acitivty]

   @doc false
   def changeset(user, attrs) do
     if Map.get(user, :id) do
         user
         |> cast(attrs, Enum.concat(@required, @optional))
         |> validate_format(:email, ~r/@/)
         |> validate_length(:password, min: 8)
         |> validate_confirmation(:password)
         |> unique_constraint(:email)
         |> validate_required(@required)

     else
         user
         |> cast(attrs, Enum.concat(@required, @optional))
         |> put_uid
         |> validate_format(:email, ~r/@/)
         |> validate_length(:password, min: 8)
         |> validate_confirmation(:password)
         |> hash_password
         |> unique_constraint(:email)
         |> validate_required(@required)

     end
   end

   def changepass(user, attrs) do
       user
       |> cast(attrs, Enum.concat(@required, @optional))
       |> put_uid
       |> validate_format(:email, ~r/@/)
       |> validate_length(:password, min: 8)
       |> validate_confirmation(:password)
       |> hash_password
       |> unique_constraint(:email)
       |> validate_required(@required)
   end

   defp hash_password(%{valid?: false} = changeset), do: changeset
   defp hash_password(%{valid?: true} = changeset) do
      hashed_password = Comeonin.Bcrypt.hashpwsalt(Ecto.Changeset.get_field(changeset, :password))
      Ecto.Changeset.put_change(changeset, :password, hashed_password)
   end

   def put_uid(changeset) do
     Ecto.Changeset.put_change(changeset, :uid, Ecto.UUID.generate)
   end

end
