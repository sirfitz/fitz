defmodule Filter do

  def file do
    var = "Tamz"
    File.stream!("/Users/romario/Desktop/names.txt")
    |> Stream.map(&String.strip/1)
    |> Stream.with_index
    |> Stream.map(fn ({line, index}) ->
      if line == var do
        IO.puts "#{index + 1} #{line}"
      end
    end)
    |> Stream.run
  end

end
