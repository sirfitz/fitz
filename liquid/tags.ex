defmodule LiquidTags do

  alias Liquid.{Template, Tag}
  alias Fitzapi.MealPlanner.{Recipe, Meal, User, Ingredient}
  alias Fitzapi.Repo

 defmodule MinusOne do
   def parse(%Tag{}=tag, %Template{}=context) do
     {tag, context}
   end

   def render(output, tag, context) do
     number = tag.markup |> Integer.parse |> elem(0)
     {["#{number - 1}"] ++ output, context}
   end
 end

 defmodule Partial do
   import Ecto.Query
   def parse(%Tag{}=tag, %Template{}=context) do

     {tag, context}
   end

    def render(output, tag, context) do

      post =
        Fitz.Content.Template
        |> where([p], not is_nil(p.id))
        |> where([p], p.title == ^tag.markup)
        |> where([p], not is_nil(p.body))
        |> limit(1)
        |> select([p], p.body)
        |> Fitz.Repo.one

      [post] ++ output, context}
    end

  end

  defmodule Collection do
    import Ecto.Query
    def parse(%Tag{}=tag, %Template{}=context) do
      {tag, context}
    end

    def render(output, tag, context) do
      params = Fitz.Map.page_params(context.assigns["params"])
      user = context.assigns["current_user"]
        c_string =
          tag.markup
          |> String.trim
          |> String.split(",")
          |> Enum.map(fn(x)-> String.trim(x) end)
          |> Enum.filter(fn(x)-> x != "" end)

        name = c_string |> Enum.at(0) |> String.replace(",", "")

        queries =
          c_string
          |> Enum.slice(1..-1)

        queries =
          queries
          |> Enum.map(fn x-> String.split(x, ":") |> Enum.map(fn(x)-> String.trim(x) end) end)

        queries =
          queries
          |> validate_queries

          values =
            case name do
              "recipes" ->
                queries
                |> rep_query(user, params)
              "meals" ->
                queries
                |> meal_query(user, params)
              "users" ->
                queries
                |> user_query(user, params)
              "ingredients" ->
                queries
                |> ing_query(user, params)
              _ ->
                [%{}]
            end
          %{name => values}

      assigns =
        context.assigns
        |> Map.put(name, values)
        |> Map.put("count", Enum.count(values))

      context = context |> Map.put(:assigns, assigns)
      {output, context}
    end

    def user_query(list, user, params) do

    end

    def validate_queries(string) do

      string = Enum.map(string, fn(x)-> Enum.map(x, fn(y)-> String.replace(y, ["!","@","#","$","%","^","&","*","(",")","-","=","+","|",";",",","'",":",",",".","<",">","?","'"], "") end) end)
      string = if Enum.member?(string, ["desc"]) do (string -- [["desc"]]) ++ [["desc", "true"]] else string end
      string = if Enum.member?(string, ["asc"]) do (string -- [["asc"]]) ++ [["asc", "true"]] else string end

      valid_queries = ["order","limit","desc","asc"]

      string
      |> Enum.map(fn(x) ->
  	if Enum.count(x) != 2 do
  	 x = Enum.slice(x, 0, 1)
  	 x =
  	   if Enum.count(x) < 2 do
  	    x = x ++ ["nil"]
  	   else
  	    x
  	   end
  	else
  	 x
  	end
        end)
      |> Enum.filter(fn [k, v] -> Enum.member?(valid_queries, k) end)
      |> Enum.map(fn [k, v]-> [k, String.replace(v, ",", "")] end)
    end

    def query(list) do
      Fitz.Content.Post
      |> where([p], not is_nil(p.id) and p.id in fragment("SELECT id FROM posts order by id desc limit 9"))
      |> Fitz.Repo.all
    end

    def finquer(list, site, kind) do
      IO.inspect list
      filters =
        list
        |> Enum.map(fn [k, v]-> %{k => v} end)
        |> Enum.reduce(fn(x,acc)-> Map.merge(x,acc) end)

      posts =
        Fitz.Content.Post
        |> where([p], not is_nil(p.id))
        |> where([p], p.site_id == ^site.id)
        |> where([p], p.type == ^kind)

      posts =
        if filters["tag"] do
          posts
          |> where([p], ^filters["tag"] in p.tags)
        else
          posts
        end
      keys =  %Fitz.Content.Post{} |> Map.keys
      order = if filters["order"] do String.to_atom(filters["order"]) else :"" end
      posts = if Enum.member?(keys, order) do
                posts =
                  if filters["desc"] do
                      posts
                      |> order_by([desc: ^order])
                  else
                    posts
                  end
                posts =
                  if filters["asc"] do
                      posts
                      |> order_by([asc: ^order])
                  else
                    posts
                  end
              else
                posts
                |> order_by([desc: :id])
              end

      posts =
        if filters["limit"] do
          posts
          |> limit(^filters["limit"])
        else
          posts
        end

      posts =
        posts
        |> Fitz.Repo.all

      posts
    end

   end

end
