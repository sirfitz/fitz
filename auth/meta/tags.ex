defmodule Meta.Tags do

  def meta_tags(post, site) do
 	meta =
	    if post != %{} && post do
      "
      <meta name=\"description\" content=\"#{post.teaser || site.about}\" />
    	<meta name=\"keywords\" content=\"#{post.meta["keywords"] || site.meta_keywords}\" />
    	<meta name=\"author\" content=\"Unparalleled Programmers\" />
      #{app_meta(site)}
    	<meta property=\"og:title\" content=\"#{post.meta["title"] || post.title || site.meta_title}\"/>
    	<meta property=\"og:image\" content=\"#{post.cover_image || site.logo}\"/>
      #{if site.ssl do
          "<meta property=\"og:url\" content=\"https://#{site.domain <> post.permalink}\"/>"
        else
          "<meta property=\"og:url\" content=\"http://#{site.domain <> post.permalink}\"/>"
        end
      }
      <meta property=\"og:site_name\" content=\"#{site.name}\"/>
      <meta property=\"og:updated_time\" content=\"#{site.updated_at}\"/>
    	<meta property=\"og:description\" content=\"#{post.teaser || site.about}\"/>
    	<meta name=\"twitter:title\" content=\"#{post.meta["title"] || post.title}\" />
    	<meta name=\"twitter:image\" content=\"#{post.cover_image || site.logo}\" />
    	<meta name=\"twitter:url\" content=\"#{site.domain <> post.permalink}\" />
    	<meta name=\"twitter:card\" content=\"#{post.teaser || site.about}\" />
      "
      #<meta name=\"og:see_also\" content=\"related_posts\"/>
      #<meta name=\"og:audio\" content=\"related_posts\"/> show if post is audio of has audio
    else
      meta_tags(site)
    end
#	   meta = String.replace(meta,[~s("),","],"")
  end

  def og_type(post) do
    types =
      %{
        2 => "article",
        3 => "product",
        4 => "article",
        5 => "article",
        6 => "music.song",
        7 => "video.other",
        8 => "article",
        9 => "article",
        10 => "place",
        11 => "article",
        12 => ":collection" #look into expading this to correspond with the collections at https://developers.facebook.com/docs/reference/opengraph
        }
    Map.get(types, post.type)
  end

  def meta_tags(site) do
      "
      <meta name=\"description\" content=\"#{site.about}\" />
      <meta name=\"keywords\" content=\"#{site.meta_keywords}\" />
      <meta name=\"author\" content=\"Unparalleled Programmers\" />
      #{app_meta(site)}
      <meta property=\"og:title\" content=\"#{site.meta_title}\"/>
      <meta property=\"og:image\" content=\"#{site.logo}\"/>
      #{if site.ssl do
          "<meta property=\"og:url\" content=\"https://#{site.domain}\"/>"
        else
          "<meta property=\"og:url\" content=\"http://#{site.domain}\"/>"
        end
      }
      <meta property=\"og:site_name\" content=\"#{site.name}\"/>
      <meta property=\"og:description\" content=\"#{site.about}\"/>
      <meta name=\"twitter:title\" content=\"#{site.name}\" />
      <meta name=\"twitter:image\" content=\"#{site.logo}\" />
      <meta name=\"twitter:url\" content=\"#{site.domain}\" />
      <meta name=\"twitter:card\" content=\"#{site.about}\" />
      "
  end

  def app_meta(site) do
    #reference FAST.COM
     "<link rel=\"icon\" type=\"image/png\" href=\"#{site.logo}\" />
     <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"#{site.logo}\">
     <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"#{site.logo}\">
     <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"#{site.logo}\">
     <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"#{site.logo}\">
     <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"#{site.logo}\">
     <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"#{site.logo}\">
     <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"#{site.logo}\">
     <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"#{site.logo}\">
     <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"#{site.logo}\">
     <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\"  href=\"#{site.logo}\">
     <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"#{site.logo}\">
     <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"#{site.logo}\">
     <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"#{site.logo}\">
     <link rel=\"icon\" href=\"#{site.logo}\" type=\"image/x-icon\" />
     <link rel=\"shortcut icon\" href=\"#{site.logo}\" type=\"image/x-icon\" />
     <meta name=\"msapplication-TileColor\" content=\"#ffffff\">
     <meta name=\"msapplication-TileImage\" content=\"#{site.logo}\">
     <meta name=\"theme-color\" content=\"#ffffff\">"
     #<link rel=\"manifest\" href=\"/assets/favicons/manifest.json\">
  end


end
