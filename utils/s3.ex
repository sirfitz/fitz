defmodule Utils.AmazonS3 do

  def bucket do
    Application.get_env(:arc, :bucket)
  end

  def upload(source, destination, content_type) do
    ExAws.S3.put_object(bucket(), destination, source, [{:acl, :public_read}, {:content_type, content_type}])
    |> ExAws.request()
  end


  def file_upload(params) do
    destination = "/file/#{params["file"].filename}_#{Timex.now |> Timex.to_gregorian_microseconds}#{Path.extname(params["file"].filename)}"
    source = params["file"].path
    content_type = params["file"].content_type
    {:ok, file_binary} = File.read(source)
    name = params["file"].filename

    link_url = "https://#{bucket()}.s3.amazonaws.com#{destination}" |> String.replace(" ", "+")
    # upload
    IO.inspect destination
    case upload(file_binary, destination, content_type) do
      {:ok, post} ->
            IO.inspect post
            url = link_url
          file = %{url: url, url_path: url, html: source, mime_type: content_type, permalink: destination, name: name}
          {:ok, file}
      {:error, reason} ->
          {:error, reason}
    end

  end
end
