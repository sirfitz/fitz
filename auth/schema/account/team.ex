defmodule Data.Account.Team do
  use Ecto.Schema
  import Ecto.Changeset


  schema "teams" do
    field :about, :string
    field :details, :map
    field :name, :string
    field :photo, :string
    field :preset, :integer
    field :uid, :string
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(team, attrs) do
    team
    |> cast(attrs, [:about, :details, :name, :photo, :preset, :uid, :user_id])
    |> put_uid
    |> validate_required([:about, :details, :name, :photo, :preset, :uid, :user_id])
  end

  def put_uid(changeset) do
    Ecto.Changeset.put_change(changeset, :uid, Ecto.UUID.generate)
  end

end
