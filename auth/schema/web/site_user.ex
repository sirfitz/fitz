defmodule Data.Web.SiteUser do
  use Ecto.Schema
  import Ecto.Changeset
  alias Data.Web.SiteUser

  schema "site_users" do
    field :referer_id, :integer
    field :role, :integer
    field :site_id, :integer

    belongs_to :user, Data.Account.User
    timestamps()
  end

  @doc false
  def changeset(site_user, attrs) do
    site_user
    |> cast(attrs, [:user_id, :site_id, :referer_id, :role])
    |> validate_required([:user_id, :site_id, :role])
  end

end
