defmodule Data.Cloud do
  @moduledoc """
  The Cloud context.
  """

  import Ecto.Query, warn: false
  alias Data.Repo

  alias Data.Cloud.File



   def aws_access_key_id() do
    #get s3 access_key
     Application.get_env(:s3_direct, :aws)[:access_key_id]
   end

   def aws_secret_key() do
     #get s3 secret_key
     Application.get_env(:s3_direct, :aws)[:secret_key]
   end

   defp hmac_sha1(secret, msg) do
     #encode values for s3
      :crypto.hmac(:sha, secret, msg)
        |> Base.encode64
   end

   defp now_plus(minutes) do
     #convert time to proper format for s3
   import Timex

     now
       |> shift(minutes: minutes)
       |> format!("{ISO:Extended:Z}")
   end

   def sign(filename, mimetype, file) do
     #create the policy for authorization
     policy = policy(filename, mimetype, file)

     %{
       key: filename,
       'Content-Type': mimetype,
       acl: "public-read",
       # this is where s3 will send the response back to
       #success_action_redirect: "http://localhost:4000/file/view?id=#{file.id}",
       success_action_status: "201",
       action: bucket_url(),
       'AWSAccessKeyId': aws_access_key_id(),
       policy: policy,
       signature: hmac_sha1(aws_secret_key(), policy)
     }
   end

   # This function is entirely cribbed from the blog post.  Generally, we convert
   # the current time to seconds, add the appropriate number of minutes, and turn
   # that into an ISO-8601 string

   # and here's our policy - we just provide an expiration and some conditions
   defp policy(key, mimetype, file, expiration_window \\ 60) do
     %{
       # This policy is valid for an hour by default.
       expiration: now_plus(expiration_window),
       conditions: [
         # You can only upload to the bucket we specify.
         %{ bucket: bucket_name() },
         # The uploaded file must be publicly readable.
         %{ acl: "public-read"},
         # You have to upload the mime type you said you would upload.
         ["starts-with", "$Content-Type", mimetype],
         # You have to upload the file name you said you would upload.
         ["starts-with", "$key", key],
         # When things work out ok, AWS should send a 201 response.
         %{ success_action_status: "201" },
        # %{ success_action_redirect: "http://localhost:4000/file/view?id=#{file.id}"}
       ]
     }
     # Let's make this into JSON.
     |> Poison.encode!
     # We also need to base64 encode it.
     |> Base.encode64
   end

   defp bucket_name() do
     # get s3 bucket name
     Application.get_env(:s3_direct, :aws)[:bucket_name]
   end

   defp bucket_region() do
     #get s3 bucket region
     Application.get_env(:s3_direct, :aws)[:region]
   end

   defp bucket_url() do
     #get s3 bucket url
     "http://#{bucket_name()}"
   end

   def file_full_url(key) do
     #get the direct file url from s3
     "http://#{bucket_name}/#{key}"
   end


  @doc """
  Returns the list of files.

  ## Examples

      iex> list_files()
      [%File{}, ...]

  """
  def list_files(site, params) do
    file =
      File
      |> where([t], t.site_id == ^site.id)
      |> where([f], is_nil(f.source) or f.source != "template" or (f.source == "template" and ilike(f.mime_type, "image/%")))

    file =
      if params["q"] && params["q"] != "" do
        q = "%#{params["q"]}%"
        file
        |> where([t], ilike(t.name, ^q))
      else
        file
      end

    file =
      if params["type"] && params["type"] != "" do
        type = "%" <> Data.mime_types(params["type"])
        file
        |> where([t], ilike(t.name , ^type))
      else
        file
      end

    file
    |> order_by([asc: :id])
    |> Repo.paginate(params)
    #(File)
  end

  def update_files() do
    File
    |> where([f], f.site_id > 1)
    |> Data.Repo.update_all(set: [source: "template"])
  end

  @doc """
  Gets a single file.

  Raises `Ecto.NoResultsError` if the File does not exist.

  ## Examples

      iex> get_file!(123)
      %File{}

      iex> get_file!(456)
      ** (Ecto.NoResultsError)

  """
  def get_file!(id), do: Repo.get!(File, id)

  @doc """
  Creates a file.

  ## Examples

      iex> create_file(%{field: value})
      {:ok, %File{}}

      iex> create_file(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_file(attrs \\ %{}) do
    %File{}
    |> File.changeset(attrs)
    |> Repo.insert()
  end

  def create_file_multi(list) do
    Repo.insert_all(File, list)
  end
  @doc """
  Updates a file.

  ## Examples

      iex> update_file(file, %{field: new_value})
      {:ok, %File{}}

      iex> update_file(file, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_file(%File{} = file, attrs) do
    file
    |> File.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a File.

  ## Examples

      iex> delete_file(file)
      {:ok, %File{}}

      iex> delete_file(file)
      {:error, %Ecto.Changeset{}}

  """
  def delete_file(%File{} = file) do
    Repo.delete(file)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking file changes.

  ## Examples

      iex> change_file(file)
      %Ecto.Changeset{source: %File{}}

  """
  def change_file(%File{} = file) do
    File.changeset(file, %{})
  end
end
