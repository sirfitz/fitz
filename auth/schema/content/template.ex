defmodule Data.Content.Template do
  use Ecto.Schema
  import Ecto.Changeset


  schema "templates" do
    field :body, :string
    field :layout_id, :integer
    field :permalink, :string
    field :site_id, :integer
    field :sub_type, :integer
    field :title, :string
    field :type, :integer
    field :user_id, :integer
    field :url, :string
    field :source, :string
    field :mime_type, :string
    field :file, :string
    field :meta, :map

    timestamps()
  end

  @doc false
  def changeset(template, attrs) do
    template
    |> cast(attrs, [:mime_type, :meta, :file, :source, :user_id, :site_id, :type, :body, :title, :url, :layout_id, :sub_type, :permalink])
    |> Changeset.put_permalink
    |> validate_required([:user_id, :site_id, :type, :title])
  end
end
