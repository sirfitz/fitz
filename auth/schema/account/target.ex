defmodule Data.Account.Target do
  use Ecto.Schema
  import Ecto.Changeset
  alias Data.Account.Target


  schema "targets" do
    field :kind, :integer
    field :kind_id, :integer
    field :name, :string
    field :provider, :string
    field :sequence_number, :integer
    field :settings, :map
    field :status, :integer
    field :tags, {:array, :integer}
    field :uid, :string
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(%Target{} = target, attrs) do
    target
    |> cast(attrs, [:name, :kind, :kind_id, :user_id, :provider, :settings, :status, :sequence_number, :tags, :uid])
    |> put_uid
    |> validate_required([:name, :kind, :kind_id, :user_id, :provider, :settings, :status, :sequence_number, :tags, :uid])
  end

  def put_uid(changeset) do
    Ecto.Changeset.put_change(changeset, :uid, Ecto.UUID.generate)
  end

end
