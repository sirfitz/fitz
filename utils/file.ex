defmodule Files do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Mms.Cloud.Post
  @verified_fields [:title, :type, :permalink, :body, :author_id,
                    :cover_image, :currency_id,
                    :external_link, :permalink, :price, :tags,
                    :teaser, :unit, :views, :mime_type]


#                    mix phx.gen.html Content Lead leads user_id:integer  referer_id:integer  body:text  place_id:integer  site_id:integer  status:integer  notified_at:datetime  title:string  tags:array:string  source:string  parent_id:integer  utm:map data:map

  def posts_download(params) do
    posts =
      Mms.Content.Post
      |> where([p], p.type == ^params["type"])
      |> where([p], p.site_id == ^params["site_id"])
      |> select([p], map(p, ^@verified_fields))
      |> Mms.Repo.all
      |> Enum.map(fn(p)->
          p =
            p
            |> Map.put(:type, Data.get_post_type(p.type))
            |> Map.values
            |> Enum.map(fn(x)-> if x == nil do "n/a" else x end end)
          end)
    filename = "#{params["site_id"]}" <> "post-#{date}"
    ext = ".csv"
    path = "/tmp/"
    f = File.open!(path <> filename <> ext, [:write])
    posts =
    Enum.concat([post_fields], posts)
    IO.inspect posts
    posts
    |> Stream.map(fn(post)-> post end)
    |> CSVLixir.write
    |> Stream.each(&(IO.write(f, &1)))
    |> Stream.run
    File.close(f)

    case Utils.AmazonS3.file_upload_direct(
    %{"file" => %{filename: filename,
      path: path <> filename,
      mime_type: "text/csv"}}
      ) do
    {:ok, file} ->
        {:ok, file.url}
    {:error, reason} ->
        {:error, reason}
    end

  end

  def date do
    DateTime.utc_now |> DateTime.to_string |> String.slice(0, 10)
  end

  def post_fields() do
     Enum.map(@verified_fields, fn(x)-> Atom.to_string(x) end)
  end

end
