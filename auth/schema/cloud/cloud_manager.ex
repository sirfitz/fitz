defmodule Data.CloudManager do
  @moduledoc """
  The Cloud context.
  """

  import Ecto.Query, warn: false
  alias Data.Repo

  alias Data.Cloud.File
  alias Data.Cloud

  def create(user, params) do
    if Enum.count(params["file"]) > 1 do
      list =
        Enum.map(params["file"], fn(x) ->
            file =
                x
                |> file_upload()
                |> set_user(user)
        end)
        Cloud.create_file_multi(list)
    else
      file =
          params
          |> file_upload
          |> set_user(user)
      changeset = File.changeset(%File{}, file)
      case Repo.insert(changeset) do
        {:ok, file} ->
          {:ok, file}
        {:error, changeset} ->
          {:error, changeset}
      end
    end
  end

  def file_upload(params), do: Utils.AmazonS3.file_upload(params)

  def set_user(file, user) do
    case user do
      nil ->
          file
      user ->
          Map.put(file, :user_id, user.id)
    end
  end

end
