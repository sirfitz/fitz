defmodule Types.Template do

  def name do
    %{
    "Layout" => 4,
    "HTML" => 5,
    "Partial" => 6,
    "Landing" => 7,
    "CSS" => 8,
    "Javascript" => 9,
    "Generic" => 10
    }
  end

  def num do
    name
    |> Fitz.Map.reverse
  end

  def get(type) do
    types =
      %{
        2 => ":blog",
        3 => ":product",
        4 => ":landing",
        5 => ":picture",
        6 => ":audio",
        7 => ":video",
        8 => ":page",
        9 => ":testimonial",
        10 => ":location",
        11 => ":event",
        12 => ":collection"
        }
    Map.get(types, type)
  end


end
