defmodule Data.CurrentTeam do
  import Plug.Conn
  import Guardian.Plug
  import Ecto.Query

  def init(opts), do: opts

  def call(conn, _opts) do
    current_site_token = conn.cookies["current_site"]
    user = conn.assigns[:current_user]

    current_team = if current_site_token do
                      {:ok, claims} = Data.Guardian.decode_and_verify(current_site_token)
                      {:ok, site} = Data.GuardianSerializer.from_token("Site:" <> claims["sub"])
		    if site do
                      if user do
                          user_site =
                                Data.Web.SiteUser
                                |> where([s], s.site_id == ^site.id and s.user_id == ^user.id)
                                |> Data.Repo.one
                          case user_site do
                            nil ->
                              nil
                            usite ->
                              site
                          end
                        else
                          nil
                        end
		   else
		    nil
		   end
                    else
                      nil
                    end

    assign(conn, :current_team, current_team)
  end

  def set_team(conn, site) do
     { :ok, jwt, full_claims } = Data.Guardian.encode_and_sign(site)

     time_in_secs_from_now = 7 * 24 * 60 * 60 # a week from now
     conn
     |> put_resp_cookie("current_site", jwt, max_age: time_in_secs_from_now)
  end
end
