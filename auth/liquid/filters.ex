defmodule LiquidFilters do
  require  Integer

  def resize(image, size, gravity \\ nil) do
    IO.puts "FILTERING"
    IO.inspect size
    "/flow/#{size}#{if gravity do "/" <> gravity <> "/" else "/" end}#{image}"
  end

  def custom_date(date) do
    Timex.format!(date, "%d.%m.%Y", :strftime)
  end

  def add_ten(value) do
    value + 10
  end

  def add(value, to_add) do
    value + to_add
  end

  def add(value, to_sub) do
    value + to_sub
  end

  def replace(value, replace, replacement) do
    String.replace(value, replace, replacement)
  end

  def split(value, splitter) do
    String.split(value, splitter)
  end

  def slice(value, start, fin) do
    String.slice(value, start..fin)
  end

  def capitalize(value) do
    String.capitalize(value)
  end

  def uppercase(value) do
    String.upcase(value)
  end

  def lowercase(value) do
    String.downcase(value)
  end

  def starts_with?(value, key) do
    String.starts_with?(value, key)
  end

  def is_even(value) do
    {v, e} = Integer.parse("#{value}")
    Integer.is_even(v)
  end

  def is_odd(value) do
    {v, e} = Integer.parse("#{value}")
    Integer.is_odd(v)
  end

  def floor_div(value, fd) do
    {v, e} = Integer.parse("#{value}")
    Integer.floor_div(v, fd)
  end

  def mod(value, mod) do
    {v, e} = Integer.parse("#{value}")
    Integer.mod(v, mod)
  end

  def gcd(value, gcd) do
    {v, e} = Integer.parse("#{value}")
    Integer.gcd(v, gcd)
  end

  def ends_with?(value, key) do
    String.ends_with?(value, key)
  end

  def invert(value) do
    inspect(value)
    |> String.reverse
  end

end
