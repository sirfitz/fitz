defmodule Data.Content.Post do
  use Ecto.Schema
  import Ecto.Changeset
  alias Data.Content.Post


  schema "posts" do
    field :author_id, :integer
    field :title, :string
    field :body, :string
    field :category_id, :integer
    field :cover_image, :string
    field :currency_id, :integer
    field :external_link, :string
    field :layout_id, :integer
    field :permalink, :string
    field :price, :integer
    field :status, :integer
    field :tags, {:array, :string}
    field :teaser, :string
    field :type, :integer
    field :unit, :string
    field :views, :integer
    field :site_id, :integer
    field :mime_type, :string
    field :meta, :map
    field :file, :string
    field :quantity, :integer
    field :sold, :integer
    field :variants, :map

    timestamps()
  end

  @required [:title, :type, :permalink]

  @optional [:permalink, :body, :author_id, :category_id, :cover_image,
             :currency_id, :external_link, :layout_id, :permalink, :price,
             :status, :tags, :teaser, :unit, :views, :site_id, :mime_type,
             :file, :meta, :quantity, :sold, :variants]

  @doc false
  def changeset(%Post{} = post, attrs) do
    post
    |> cast(attrs, Enum.concat(@required, @optional))
    |> put_mime
    |> put_uid
    |> Changeset.put_permalink
    |> validate_required(@required)
  end

  def put_uid(changeset) do
    Ecto.Changeset.put_change(changeset, :uid, Ecto.UUID.generate)
  end

  def put_mime(changeset) do

    if Ecto.Changeset.get_field(changeset, :permalink) do
      if Path.extname(Ecto.Changeset.get_field(changeset, :permalink)) == ""  do
          Ecto.Changeset.put_change(changeset, :mime_type, "text/html")
      else
        Ecto.Changeset.put_change(changeset, :mime_type, process_mime(changeset))
      end
    else
      changeset
    end
  end

  def process_mime(changeset) do
    permalink = Ecto.Changeset.get_field(changeset, :permalink)
    "." <> extname = Path.extname permalink
    MimeType.get(extname)
  end

end
