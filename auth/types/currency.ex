defmodule Data.Currency do

  def all do
    [
      %{
      "id" => 1,
      "name" => "JMD"
      },
      %{
        "id" => 2,
        "name" => "USD"
      },
      %{
        "id" => 3,
        "name" => "CAD"
      },
      %{
        "id" => 4,
        "name" => "EUR"
      },
      %{
        "id" => 5,
        "name" => "BPD"
      }
    ]
  end

end
