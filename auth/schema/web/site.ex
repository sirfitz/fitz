defmodule Data.Web.Site do
  use Ecto.Schema
  import Ecto.Changeset
  alias Data.Web.Site


  schema "sites" do
    field :about, :string
    field :addrees, :string
    field :alt_domains, :string
    field :city, :string
    field :cname, :string
    field :country, :string
    field :currency_id, :integer
    field :deleted, :boolean, default: false
    field :domain, :string
    field :email, :string
    field :favicon, :string
    field :fax, :string
    field :language, :string
    field :logo, :string
    field :meta_description, :string
    field :meta_keywords, :string
    field :meta_title, :string
    field :mobile, :string
    field :name, :string
    field :origin, :string
    field :owner_id, :integer
    field :postal_code, :string
    field :region, :string
    field :ssl, :boolean, default: false
    field :status, :integer
    field :template_id, :string
    field :timezone, :string

    timestamps()
  end

  @required  [:name, :domain, :email]
  @optional  [:cname, :alt_domains, :about, :logo,
              :currency_id, :addrees, :city, :region, :country, :ssl,
              :owner_id, :postal_code, :mobile, :fax, :status,
              :language, :deleted, :timezone, :template_id,
              :meta_description, :meta_title, :meta_keywords,
              :favicon, :origin]
  @doc false
  def changeset(%Site{} = site, attrs) do
    site
    |> put_uid
    |> validate_format(:email, ~r/@/)
    |> validate_required(@required)
  end

  def put_uid(changeset) do
    Ecto.Changeset.put_change(changeset, :uid, Ecto.UUID.generate)
  end

end
