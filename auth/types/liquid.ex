defmodule Types.Liquid do

  def get(type) do
    types =
      %{
        "audio" => 6,
        "blogs" => 2,
        "collections" => 12,
        "events" => 11,
        "landings" => 4,
        "locations" => 10,
        "pages" => 8,
        "pictures" => 5,
        "products" => 3,
        "testimonials" => 9,
        "videos" => 7
       }
    Map.get(types, type)
  end

end
