defmodule Utils.AmazonS3 do

  def bucket do
    Application.get_env(:ex_aws, :bucket)
  end

  def upload(source, destination, content_type) do
    ExAws.S3.put_object(bucket(), destination, source, [{:acl, :public_read}, {:content_type, content_type}])
    |> ExAws.request()
  end

  def cdn_upload(params) do
    destination = "/cdn/#{Fitz.sluggify(params["file"].filename)}"
    source = params["file"].path
    content_type = Map.get(params["file"], :mime_type) || Map.get(params["file"], :content_type)
    {:ok, file_binary} = File.read(source)
    name = params["file"].filename
    link_url = "https://d1l54leyvskqrr.cloudfront.net#{destination}"
    # upload
    case upload(file_binary, destination, content_type) do
      {:ok, _} ->
            url = link_url
          file = %{url: url, url_path: url, html: source, mime_type: content_type, permalink: destination, name: name}
          {:ok, file}
      {:error, reason} ->
          {:error, reason}
    end
  end

  def static_upload(params) do
    params =  Fitz.Map.atom_keys_to_string(params)
    site_id = UUID.uuid4 |> String.slice(24..-1)
    destination = "/#{site_id}/cdn/#{Fitz.sluggify(params["file"].filename)}"
    source = params["file"].path
    content_type = Map.get(params["file"], :mime_type) || Map.get(params["file"], :content_type)
    {:ok, file_binary} = File.read(source)
    name = params["file"].filename
    link_url = "https://d1l54leyvskqrr.cloudfront.net#{destination}"
    # upload
    case upload(file_binary, destination, content_type) do
      {:ok, _} ->
            url = link_url
          file = %{url: url, url_path: url, html: source, mime_type: content_type, permalink: destination, name: name}
          {:ok, file}
      {:error, reason} ->
          {:error, reason}
    end
  end

  def static_upload(site_id, params) do
    params =  Fitz.Map.atom_keys_to_string(params)
    site_id = site_id |> String.slice(24..-1)
    destination = "/#{site_id}/cdn/#{Fitz.sluggify(params["file"].filename)}"
    source = params["file"].path
    content_type = Map.get(params["file"], :mime_type) || Map.get(params["file"], :content_type)
    {:ok, file_binary} = File.read(source)
    name = params["file"].filename
    link_url = "https://d1l54leyvskqrr.cloudfront.net#{destination}"
    # upload
    case upload(file_binary, destination, content_type) do
      {:ok, _} ->
            url = link_url
          file = %{url: url, url_path: url, html: source, mime_type: content_type, permalink: destination, name: name}
          {:ok, file}
      {:error, reason} ->
          {:error, reason}
    end
  end

  def file_upload(params) do
    params =  Fitz.Map.atom_keys_to_string(params)
    site_id = UUID.uuid4 |> String.slice(24..-1)
    destination = "/#{site_id}/file/#{Fitz.file_slug(params["file"].filename)}"
    source = params["file"].path
    content_type = Map.get(params["file"], :mime_type) || Map.get(params["file"], :content_type)
    {:ok, file_binary} = File.read(source)
    name = params["file"].filename
    link_url = "https://d1l54leyvskqrr.cloudfront.net#{destination}"
    # upload
    case upload(file_binary, destination, content_type) do
      {:ok, _} ->
            url = link_url
          file = %{url: url, url_path: url, html: source, mime_type: content_type, permalink: destination, name: name}
          {:ok, file}
      {:error, reason} ->
          {:error, reason}
    end

  end

  def file_upload(site_id, params) do
    params =  Fitz.Map.atom_keys_to_string(params)
    site_id = site_id |> String.slice(24..-1)
    destination = "/#{site_id}/file/#{Fitz.file_slug(params["file"].filename)}"
    source = params["file"].path
    content_type = Map.get(params["file"], :mime_type) || Map.get(params["file"], :content_type)
    {:ok, file_binary} = File.read(source)
    name = params["file"].filename
    link_url = "https://d1l54leyvskqrr.cloudfront.net#{destination}"
    # upload
    case upload(file_binary, destination, content_type) do
      {:ok, _} ->
            url = link_url
          file = %{url: url, url_path: url, html: source, mime_type: content_type, permalink: destination, name: name}
          {:ok, file}
      {:error, reason} ->
          {:error, reason}
    end

  end

  def template_upload(file) do
    file = Fitz.Map.string_keys_to_atom(file)
    destination = "/templates/folder/#{Fitz.file_slug(file.filename)}"
    source = file.path
    content_type = Map.get(file, :mime_type) || Map.get(file, :content_type)
    {:ok, file_binary} = File.read(source)
    name = file.filename

    link_url = "https://d1l54leyvskqrr.cloudfront.net#{destination}"
    # upload
    case upload(file_binary, destination, content_type) do
      {:ok, _} ->
          file = %{url: link_url, url_path: link_url, html: source, mime_type: content_type, permalink: destination, name: name}
          {:ok, file}
      {:error, reason} ->
          {:error, reason}
    end
  end

  def folder_upload(file) do
    file = Fitz.Map.string_keys_to_atom(file)
    destination = "/folders/#{Fitz.file_slug(file.filename)}"
    source = file.path
    content_type = Map.get(file, :mime_type) || Map.get(file, :content_type)
    {:ok, file_binary} = File.read(source)
    name = Path.basename(file.filename)
    link_url = "https://d1l54leyvskqrr.cloudfront.net#{destination}"
    # upload
    case upload(file_binary, destination, content_type) do
      {:ok, _} ->
          %{url: link_url, html: source, content: content_type, permalink: destination, filename: name}
      {:error, reason} ->
          reason
    end

  end
end
