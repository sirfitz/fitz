defmodule Data.Actions.Tag do
  use Ecto.Schema
  import Ecto.Changeset
  alias Data.Actions.Tag


  schema "tags" do
    field :name, :string
    field :team_id, :integer
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(%Tag{} = tag, attrs) do
    tag
    |> cast(attrs, [:user_id, :team_id, :name])
    |> validate_required([:name])
  end
end
