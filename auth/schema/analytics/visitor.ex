defmodule Data.Analytics.Visitor do
  use Ecto.Schema
  import Ecto.Changeset


  schema "visitors" do
    field :browser, :string
    field :agent, :string
    field :city, :string
    field :continent, :string
    field :country, :string
    field :device_brand, :string
    field :device_model, :string
    field :device_type, :string
    field :geoname_id, :integer
    field :os, :string
    field :os_platform, :string
    field :os_version, :string
    field :ip, :string
    field :site_id, :integer
    field :timezone, :string
    field :user_id, :integer
    field :visitor_id, :string
    field :request_path, :string
    field :referer, :string
    field :ref_domain, :string
    field :network, :string

    timestamps()
  end

  @doc false
  def changeset(visitor, attrs) do
    visitor
    |> cast(attrs, [:visitor_id, :network, :referer, :ref_domain, :request_path, :ip, :country, :city, :geoname_id, :city, :continent, :timezone, :browser, :agent, :device_brand, :device_model, :device_type, :os, :os_platform, :os_version, :site_id, :user_id])
    |> validate_required([:visitor_id, :ip, :site_id])
  end

end
