defmodule Filter do
  import Ecto.Query, warn: false
  alias Mms.Repo

  alias Mms.Food.Recipe
  alias Mms.Food.Ingredients
  alias Mms.Food.Steps


  def file do
    var = "Tamz"
    File.stream!("/Users/romario/Desktop/names.txt")
    |> Stream.map(&String.strip/1)
    |> Stream.with_index
    |> Stream.map(fn ({line, index}) ->
      if line == var do
        IO.puts "#{index + 1} #{line}"
      end
    end)
    |> Stream.run
  end

  def amounts do
    ingredients =
      Ingredients
      |> where([i], i.name != ^"" and ilike(i.amount, "% %"))
      |> where([i], not ilike(i.amount, "%fl.oz"))
      |> where([i], not ilike(i.amount, "%cup"))
      |> where([i], not ilike(i.amount, "%cups"))
      |> where([i], not ilike(i.amount, "%tsp"))
      |> where([i], not ilike(i.amount, "%tbsp"))
      |> where([i], not ilike(i.amount, "%cloves"))
      |> where([i], not ilike(i.amount, "%slices"))
      |> where([i], not ilike(i.amount, "%sprigs"))
      |> order_by(fragment("RANDOM()"))
      |> distinct([i], i.amount)
      #|> select([i], i.amount)
      |> Repo.all
      |> Enum.uniq
    #[Enum.count(ingredients), Enum.reverse(ingredients)]
  end

  def splits(amounts) do
    Enum.map(amounts, fn(x)->
      a = String.split(x, " ") |> Enum.slice(1..-1) |> Enum.join(" ")
      b = String.split(x, " ") |> Enum.slice(0..1) |> Enum.join(" ")
      [a, b]
     end)
    |> Enum.uniq
  end

  def get_amount(list, rid) do
    Enum.map(list, fn(string)->
        amount =
          if String.contains?(string, " of ") do
            enum = String.split(string, "of")
            amount =
              List.first(enum)
              |> String.trim
          else
            enum = String.split(string, " ")
            amount =
              List.first(enum)
              |> String.trim
          end

        name = String.replace(string, amount, "(single)")
        ig = %{"name" => name, "unit" => amount, "recipe_id" => rid}
        Mms.Food.create_ingredient(ig)
    end)
  end


  def splits2(amounts) do
    Enum.map(amounts, fn(x)->
      String.split(x, " ") |> Enum.slice(1..-1) |> Enum.join(" ") end)
    |> Enum.uniq
  end

  def split(record) do
    pre =
      String.split(record.amount, " ")
      |> Enum.slice(1..-1)
      |> Enum.join(" ")

    name =
      pre <> " " <> record.name

    amount =
      String.split(record.amount, " ")
      |> Enum.at(0)

    record =
      %{
        "amount" => amount,
        "name" => name,
        "o" => record.amount
        }
    #  updates(record)
    #{}"#{amount} #{name}"
  end

  def records do
    Ingredients
    |> where([i], i.name == ^"" and ilike(i.amount, "% %"))
    |> distinct([i], i.amount)
    |> Repo.all
    |> Enum.uniq
  end

  def organize() do
    Enum.map(records, fn(x)->
      #ingredients =
      #  Ingredients
      #  |> where([i], i.amount == ^x.amount)
      #  |> Repo.all
      cycle(x)
    end)
  end

  def updates(record) do
    Ingredients
    |> where([i], i.amount == ^record["o"])
    |> Repo.update_all(set: [amount: record["amount"], name: record["name"]])
  end

  def cycle(ingredients) do
    #Enum.map(ingredients, fn(x)->
      split(ingredients)
    #end)
  end

  def make_steps(steps,rid) do
    steps  =
      steps
      |> Enum.with_index(1)
      |> Enum.map(fn{k, v}->
        step =
          %{"num" => v,
            "step" => k,
            "recipe_id" => rid,
            #{}"inserted_at" => DateTime.utc_now,
            #{}"updated_at" => DateTime.utc_now
           }
          IO.inspect step
          Mms.Food.create_step(step)
          #IO.inspect Skim.Repo.insert(changeset)
         end)
  end



end
