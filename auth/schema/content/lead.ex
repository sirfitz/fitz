defmodule Data.Content.Lead do
  use Ecto.Schema
  import Ecto.Changeset


  schema "leads" do
    field :body, :string
    field :data, :map
    field :notified_at, :naive_datetime
    field :place_id, :integer
    field :referer_id, :integer
    field :site_id, :integer
    field :source, :string
    field :status, :integer
    field :tags, {:array, :string}
    field :title, :string
    field :utm, :map

    belongs_to :parent, Data.Content.Post
    belongs_to :user, Data.Account.User
    timestamps()
  end

  @required [:user_id, :title, :site_id, :status, :source]

  @optional [:referer_id, :body, :place_id, :notified_at, :tags,
             :parent_id, :utm, :data]

  @doc false
  def changeset(lead, attrs) do
    lead
    |> cast(attrs, Enum.concat(@required, @optional))
    |> put_status
    |> Changeset.put_permalink
    |> validate_required(@required)
  end

  def put_status(changeset) do
    Ecto.Changeset.put_change(changeset, :status, 1)
  end

end
