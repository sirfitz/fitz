defmodule Data.Cloud.File do
  use Ecto.Schema
  import Ecto.Changeset
  alias Data.Cloud.File


  schema "files" do
    field :category, :integer
    field :height, :integer
    field :kind, :integer
    field :length, :string
    field :mime_type, :string
    field :name, :string
    field :permalink, :string
    field :published_at, :naive_datetime
    field :size, :integer
    field :tags, {:array, :integer}
    field :thumbnail, :string
    field :uid, :string
    field :url, :string
    field :views, :integer
    field :width, :integer
    field :site_id, :integer
    field :source, :string
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(%File{} = file, attrs) do
    file
    |> cast(attrs, [:name, :site_id, :user_id, :source, :mime_type, :kind, :thumbnail, :length, :size, :height, :width, :url, :permalink, :views, :published_at, :uid, :category, :tags])
    |> put_uid
    |> validate_required([:name])
  end

  def put_uid(changeset) do
    Ecto.Changeset.put_change(changeset, :uid, Ecto.UUID.generate)
  end
end
