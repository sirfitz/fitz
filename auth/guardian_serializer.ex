defmodule Data.GuardianSerializer do
  @behaviour Guardian.Serializer
  alias Data.Repo
  alias Data.Account.User
  alias Data.Web.Site

  def for_token(user = %User{}), do: { :ok, "User:#{user.id}" }
  def for_token(site = %Site{}), do: { :ok, "Site:#{site.id}" }
  def for_token(_), do: { :error, "Unknown resource type" }

  def from_token("User:" <> id), do: { :ok, Repo.get(User, id) }
  def from_token("Site:" <> id), do: { :ok, Repo.get(Site, id) }
  def from_token(_), do: { :error, "Unknown resource type" }

end
